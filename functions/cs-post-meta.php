<?php


/*
 * Set meta_keys so we can find the post with the shortcode back.
 *
 * @since 1.0.0
 */
function chessgame_shizzle_save_post_for_shortcode_meta( $id ) {

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
		return;
	if ( defined( 'DOING_CRON' ) && DOING_CRON )
		return;

	$post = get_post( $id );

	if ( has_shortcode( $post->post_content, 'chessgame_shizzle_form' ) ) {
		// Set a meta_key so we can find the post with the shortcode back.
		$meta_value = get_post_meta( $id, 'chessgame_shizzle_form', true );
		if ( $meta_value !== 'true' ) {
			update_post_meta( $id, 'chessgame_shizzle_form', 'true' );
		}
	} else {
		// Remove the meta_key in case it is set.
		delete_post_meta( $id, 'chessgame_shizzle_form' );
	}

	if ( has_shortcode( $post->post_content, 'chessgame_shizzle_simple_list' ) ) {
		// Set a meta_key so we can find the post with the shortcode back.
		$meta_value = get_post_meta( $id, 'chessgame_shizzle_simple_list', true );
		if ( $meta_value !== 'true' ) {
			update_post_meta( $id, 'chessgame_shizzle_simple_list', 'true' );
		}
	} else {
		// Remove the meta_key in case it is set.
		delete_post_meta( $id, 'chessgame_shizzle_simple_list' );
	}

}
add_action('save_post', 'chessgame_shizzle_save_post_for_shortcode_meta');


/*
 * Make the meta field above protected, so it is not in the custom fields metabox.
 *
 * @since 1.0.0
 */
function chessgame_shizzle_is_protected_shortcode_meta( $protected, $meta_key, $meta_type ) {

	switch ($meta_key) {
		case 'chessgame_shizzle_form':
			return true;
		case 'chessgame_shizzle_simple_list':
			return true;
	}

	return $protected;
}
add_filter( 'is_protected_meta', 'chessgame_shizzle_is_protected_shortcode_meta', 10, 3 );
